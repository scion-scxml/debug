# SCION debug client

This package serves as a drop-in replacement for @scion-scxml/scxml package. It
enables source maps (to enable graphical debugging in Chrome DevTools
JavaScript debugger) and the monitor client (to automatically send events to the monitor
server).

Here is a video of this module in action:

[![Link to video](http://img.youtube.com/vi/Pg9tYuJN6BI/0.jpg)](http://www.youtube.com/watch?v=Pg9tYuJN6BI)
